<?php

namespace Drupal\inline_formatter_field\Plugin\Editor;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editor\Entity\Editor;
use Drupal\editor\Plugin\EditorBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a Ace Editor text editor for Drupal.
 *
 * @Editor(
 *   id = "iff_ace_editor",
 *   label = @Translation("Ace Editor"),
 *   supports_content_filtering = TRUE,
 *   supports_inline_editing = TRUE,
 *   is_xss_safe = FALSE,
 *   supported_element_types = {
 *     "textarea"
 *   }
 * )
 *
 * @internal
 *   Plugin classes are internal.
 */
class AceEditor extends EditorBase implements ContainerFactoryPluginInterface {

  /**
   * A config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs an Ace Editor editor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Get editor settings.
    $editor = $form_state->get('editor');
    assert($editor instanceof Editor);
    $editor_settings = $editor->getSettings();

    $complete_form_state = $form_state instanceof SubformStateInterface ? $form_state->getCompleteFormState() : $form_state;
    $subform_values = $complete_form_state->getValue(['editor', 'settings']);
    // Default settings.
    $available_themes = $subform_values['themes']['available_themes']['theme_values'] ?? $editor_settings['available_themes'] ?? $this->getDefaultAvailableThemes();
    $available_modes = $subform_values['modes']['available_modes']['mode_values'] ?? $editor_settings['available_modes'] ?? $this->getDefaultAvailableModes();
    $default_theme = $subform_values['default_theme'] ?? $editor_settings['default_theme'] ?? $this->getDefaultTheme();
    $default_mode = $subform_values['default_mode'] ?? $editor_settings['default_mode'] ?? $this->getDefaultMode();
    $available_extras = $subform_values['extras']['available_extras']['extra_values'] ?? $editor_settings['available_extras'] ?? $this->getDefaultAvailableExtras();

    // Ace Editor themes.
    $form['default_theme'] = [
      '#type' => 'value',
      '#value' => $default_theme,
    ];
    $form['themes'] = [
      '#prefix' => '<div id="settings-form-themes">',
      '#suffix' => '</div>',
    ];
    $form['themes']['available_themes'] = [
      '#type' => 'details',
      '#title' => $this->t("Themes - @theme_name (@theme_val)", [
        '@theme_name' => $available_themes[$default_theme],
        '@theme_val' => $complete_form_state->getValue('theme_table') ? $complete_form_state->getValue('theme_table')[$default_theme]['key'] : $default_theme,
      ]),
    ];
    $form['themes']['available_themes']['theme_values'] = [
      '#type' => 'value',
      '#value' => $available_themes,
    ];
    $form['themes']['available_themes']['theme_table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Key'),
        $this->t('Theme'),
        $this->t('Weight'),
        $this->t('Actions'),
      ],
      '#empty' => $this->t('Sorry, there are no themes.'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'theme-table-sort-weight',
        ],
      ],
    ];
    foreach ($available_themes as $key => $theme) {
      $weight = array_search($key, array_keys($available_themes));
      $form['themes']['available_themes']['theme_table'][$key]['#attributes']['class'][] = 'draggable';
      $form['themes']['available_themes']['theme_table'][$key]['#weight'] = $weight;
      $form['themes']['available_themes']['theme_table'][$key]['key'] = [
        '#type' => 'textfield',
        '#default_value' => $key,
        '#required' => TRUE,
      ];
      $form['themes']['available_themes']['theme_table'][$key]['name'] = [
        '#type' => 'textfield',
        '#default_value' => $theme,
        '#required' => TRUE,
      ];
      $form['themes']['available_themes']['theme_table'][$key]['weight'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Weight for @title', ['@title' => $key]),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
        '#size' => 3,
        '#attributes' => ['class' => ['theme-table-sort-weight']],
      ];
      if ($default_theme !== $key) {
        $form['themes']['available_themes']['theme_table'][$key]['actions'] = [];
        $form['themes']['available_themes']['theme_table'][$key]['actions']['default'] = [
          '#type' => 'submit',
          '#value' => $this->t("Make Default"),
          '#submit' => ['\Drupal\inline_formatter_field\Plugin\Editor\AceEditor::makeDefaultTheme'],
          '#name' => 'default-theme-' . $key,
          '#attributes' => [
            'theme_key' => $key,
          ],
          '#ajax' => [
            'wrapper' => 'settings-form-themes',
            'callback' => [$this, 'reloadThemeForm'],
            'progress' => [
              'type' => 'throbber',
              'message' => NULL,
            ],
          ],
        ];
      }
      if (count($available_themes) > 1) {
        $form['themes']['available_themes']['theme_table'][$key]['actions']['remove'] = [
          '#type' => 'submit',
          '#value' => $this->t("Remove"),
          '#submit' => ['\Drupal\inline_formatter_field\Plugin\Editor\AceEditor::removeTheme'],
          '#name' => 'remove-theme-' . $key,
          '#attributes' => [
            'theme_key' => $key,
          ],
          '#ajax' => [
            'wrapper' => 'settings-form-themes',
            'callback' => [$this, 'reloadThemeForm'],
            'progress' => [
              'type' => 'throbber',
              'message' => NULL,
            ],
          ],
        ];
      }
    }
    $form['themes']['available_themes']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t("Add theme"),
      '#name' => 'add-theme',
      '#submit' => ['\Drupal\inline_formatter_field\Plugin\Editor\AceEditor::addTheme'],
      '#ajax' => [
        'wrapper' => 'settings-form-themes',
        'callback' => [$this, 'reloadThemeForm'],
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];
    // Ace Editor modes.
    $form['default_mode'] = [
      '#type' => 'value',
      '#value' => $default_mode,
    ];
    $form['modes'] = [
      '#prefix' => '<div id="settings-form-modes">',
      '#suffix' => '</div>',
    ];
    $form['modes']['available_modes'] = [
      '#type' => 'details',
      '#title' => $this->t("Modes - @mode_name (@mode_val)", [
        '@mode_name' => $available_modes[$default_mode],
        '@mode_val' => $complete_form_state->getValue('mode_table') ? $complete_form_state->getValue('mode_table')[$default_mode]['key'] : $default_mode,
      ]),
    ];
    $form['modes']['available_modes']['mode_values'] = [
      '#type' => 'value',
      '#value' => $available_modes,
    ];
    $form['modes']['available_modes']['mode_table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Key'),
        $this->t('Mode'),
        $this->t('Weight'),
        $this->t('Actions'),
      ],
      '#empty' => $this->t('Sorry, there are no modes.'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'mode-table-sort-weight',
        ],
      ],
    ];
    foreach ($available_modes as $key => $mode) {
      $weight = array_search($key, array_keys($available_modes));
      $form['modes']['available_modes']['mode_table'][$key]['#attributes']['class'][] = 'draggable';
      $form['modes']['available_modes']['mode_table'][$key]['#weight'] = $weight;
      $form['modes']['available_modes']['mode_table'][$key]['key'] = [
        '#type' => 'textfield',
        '#default_value' => $key,
        '#required' => TRUE,
      ];
      $form['modes']['available_modes']['mode_table'][$key]['name'] = [
        '#type' => 'textfield',
        '#default_value' => $mode,
        '#required' => TRUE,
      ];
      $form['modes']['available_modes']['mode_table'][$key]['weight'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Weight for @title', ['@title' => $key]),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
        '#size' => 3,
        '#attributes' => ['class' => ['mode-table-sort-weight']],
      ];
      if ($default_mode !== $key) {
        $form['modes']['available_modes']['mode_table'][$key]['actions'] = [];
        $form['modes']['available_modes']['mode_table'][$key]['actions']['default'] = [
          '#type' => 'submit',
          '#value' => $this->t("Make Default"),
          '#submit' => ['\Drupal\inline_formatter_field\Plugin\Editor\AceEditor::makeDefaultMode'],
          '#name' => 'default-mode-' . $key,
          '#attributes' => [
            'mode_key' => $key,
          ],
          '#ajax' => [
            'wrapper' => 'settings-form-modes',
            'callback' => [$this, 'reloadModeForm'],
            'progress' => [
              'type' => 'throbber',
              'message' => NULL,
            ],
          ],
        ];
      }
      if (count($available_modes) > 1) {
        $form['modes']['available_modes']['mode_table'][$key]['actions']['remove'] = [
          '#type' => 'submit',
          '#value' => $this->t("Remove"),
          '#submit' => ['\Drupal\inline_formatter_field\Plugin\Editor\AceEditor::removeMode'],
          '#name' => 'remove-mode-' . $key,
          '#attributes' => [
            'mode_key' => $key,
          ],
          '#ajax' => [
            'wrapper' => 'settings-form-modes',
            'callback' => [$this, 'reloadModeForm'],
            'progress' => [
              'type' => 'throbber',
              'message' => NULL,
            ],
          ],
        ];
      }
    }
    $form['modes']['available_modes']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t("Add mode"),
      '#name' => 'add-mode',
      '#submit' => ['\Drupal\inline_formatter_field\Plugin\Editor\AceEditor::addMode'],
      '#ajax' => [
        'wrapper' => 'settings-form-modes',
        'callback' => [$this, 'reloadModeForm'],
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];
    // Ace Editor extra settings.
    $form['extras'] = [
      '#prefix' => '<div id="settings-form-extras">',
      '#suffix' => '</div>',
    ];
    $form['extras']['available_extras'] = [
      '#type' => 'details',
      '#title' => $this->t("Extra Options"),
    ];
    $form['extras']['available_extras']['help'] = [
      '#markup' => $this->t('A list of available options to add can be found on <a href=":url" target="_blank">Configuring Ace</a>.<br>Booleans should be entered "true" for true and "false" for false.', [
        ':url' => 'https://github.com/ajaxorg/ace/wiki/Configuring-Ace',
      ]),
    ];
    $form['extras']['available_extras']['extra_values'] = [
      '#type' => 'value',
      '#value' => $available_extras,
    ];
    $form['extras']['available_extras']['extra_table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Key'),
        $this->t('Value'),
        $this->t('Weight'),
        $this->t('Actions'),
      ],
      '#empty' => $this->t('Sorry, there are no extra options.'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'extra-table-sort-weight',
        ],
      ],
    ];
    foreach ($available_extras as $key => $value) {
      $weight = array_search($key, array_keys($available_extras));
      $form['extras']['available_extras']['extra_table'][$key]['#attributes']['class'][] = 'draggable';
      $form['extras']['available_extras']['extra_table'][$key]['#weight'] = $weight;
      $form['extras']['available_extras']['extra_table'][$key]['key'] = [
        '#type' => 'textfield',
        '#default_value' => $key,
        '#required' => TRUE,
      ];
      $form['extras']['available_extras']['extra_table'][$key]['value'] = [
        '#type' => 'textfield',
        '#default_value' => $value,
        '#required' => TRUE,
      ];
      $form['extras']['available_extras']['extra_table'][$key]['weight'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Weight for @title', ['@title' => $key]),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
        '#size' => 3,
        '#attributes' => ['class' => ['extra-table-sort-weight']],
      ];
      $form['extras']['available_extras']['extra_table'][$key]['actions']['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t("Remove"),
        '#submit' => ['\Drupal\inline_formatter_field\Plugin\Editor\AceEditor::removeExtra'],
        '#name' => 'remove-extra-' . $key,
        '#attributes' => [
          'extra_key' => $key,
        ],
        '#ajax' => [
          'wrapper' => 'settings-form-extras',
          'callback' => [$this, 'reloadExtraForm'],
          'progress' => [
            'type' => 'throbber',
            'message' => NULL,
          ],
        ],
      ];
    }
    $form['extras']['available_extras']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t("Add Extra"),
      '#name' => 'add-extra',
      '#submit' => ['\Drupal\inline_formatter_field\Plugin\Editor\AceEditor::addExtra'],
      '#ajax' => [
        'wrapper' => 'settings-form-extras',
        'callback' => [$this, 'reloadExtraForm'],
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];
    // Clear tokens.
    $clear_tokens = $subform_values['clear_tokens'] ?? $editor_settings['clear_tokens'] ?? FALSE;
    $form['clear_tokens'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove tokens from the final text if no replacement value can be generated.'),
      '#default_value' => $clear_tokens,
    ];

    return $form;
  }

  /**
   * Returns the ace_theme from the settings config.
   */
  public function getDefaultTheme() {
    return $this->configFactory->get('inline_formatter_field.settings')->get('ace_theme');
  }

  /**
   * Returns the ace_mode from the settings config.
   */
  public function getDefaultMode() {
    return $this->configFactory->get('inline_formatter_field.settings')->get('ace_mode');
  }

  /**
   * Returns the available_themes from the settings config.
   */
  public function getDefaultAvailableThemes() {
    return $this->configFactory->get('inline_formatter_field.settings')->get('available_themes');
  }

  /**
   * Returns the available_modes from the settings config.
   */
  public function getDefaultAvailableModes() {
    return $this->configFactory->get('inline_formatter_field.settings')->get('available_modes');
  }

  /**
   * Returns the extra_options from the settings config.
   */
  public function getDefaultAvailableExtras() {
    return $this->configFactory->get('inline_formatter_field.settings')->get('extra_options');
  }

  /**
   * Returns the theme part of the settings form.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   *
   * @return array
   *   The theme part of the settings form.
   */
  public function reloadThemeForm(array $form, FormStateInterface $form_state) {
    $form['editor']['settings']['subform']['themes']['available_themes']['#attributes']['open'] = 'open';
    return $form['editor']['settings']['subform']['themes'];
  }

  /**
   * Returns the mode part of the settings form.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   *
   * @return array
   *   The mode part of the settings form.
   */
  public function reloadModeForm(array $form, FormStateInterface $form_state) {
    $form['editor']['settings']['subform']['modes']['available_modes']['#attributes']['open'] = 'open';
    return $form['editor']['settings']['subform']['modes'];
  }

  /**
   * Returns the extra options part of the settings form.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   *
   * @return array
   *   The extra options part of the settings form.
   */
  public function reloadExtraForm(array $form, FormStateInterface $form_state) {
    $form['editor']['settings']['subform']['extras']['available_extras']['#attributes']['open'] = 'open';
    return $form['editor']['settings']['subform']['extras'];
  }

  /**
   * Sets the theme's key as the default theme value.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   *
   * @return array
   *   The settings form.
   */
  public static function makeDefaultTheme(array $form, FormStateInterface $form_state) {
    $theme_key = $form_state->getTriggeringElement()['#attributes']['theme_key'];
    $form_state->setValue(['editor', 'settings', 'default_theme'], $theme_key);
    $form_state->setRebuild();
    return $form;
  }

  /**
   * Sets the mode's key as the default mode value.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   *
   * @return array
   *   The settings form.
   */
  public static function makeDefaultMode(array $form, FormStateInterface $form_state) {
    $mode_key = $form_state->getTriggeringElement()['#attributes']['mode_key'];
    $form_state->setValue(['editor', 'settings', 'default_mode'], $mode_key);
    $form_state->setRebuild();
    return $form;
  }

  /**
   * Adds a new theme to the theme values.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   *
   * @return array
   *   The settings form.
   */
  public static function addTheme(array $form, FormStateInterface $form_state) {
    $available_themes = static::getThemes($form_state);
    $available_themes['key_' . uniqid()] = 'theme';
    $form_state->setValue([
      'editor',
      'settings',
      'themes',
      'available_themes',
      'theme_values',
    ], $available_themes);
    $form_state->setRebuild();
    return $form;
  }

  /**
   * Adds a new mode to the mode values.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   *
   * @return array
   *   The settings form.
   */
  public static function addMode(array $form, FormStateInterface $form_state) {
    $available_modes = static::getModes($form_state);
    $available_modes['key_' . uniqid()] = 'mode';
    $form_state->setValue([
      'editor',
      'settings',
      'modes',
      'available_modes',
      'mode_values',
    ], $available_modes);
    $form_state->setRebuild();
    return $form;
  }

  /**
   * Adds a new extra option to the extras values.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   *
   * @return array
   *   The settings form.
   */
  public static function addExtra(array $form, FormStateInterface $form_state) {
    $available_extras = static::getExtras($form_state);
    $available_extras['key_' . uniqid()] = 'value';
    $form_state->setValue([
      'editor',
      'settings',
      'extras',
      'available_extras',
      'extra_values',
    ], $available_extras);
    $form_state->setRebuild();
    return $form;
  }

  /**
   * Removes a theme set in the triggering element from the form.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   *
   * @return array
   *   The settings form.
   */
  public static function removeTheme(array $form, FormStateInterface $form_state) {
    $themes_arr = static::getThemes($form_state);
    $theme_key = $form_state->getTriggeringElement()['#attributes']['theme_key'];

    unset($themes_arr[$theme_key]);
    $form_state->setValue([
      'editor',
      'settings',
      'themes',
      'available_themes',
      'theme_values',
    ], $themes_arr);

    if ($form_state->getValue(['editor', 'settings', 'default_theme']) === $theme_key) {
      $form_state->setValue(['editor', 'settings', 'default_theme'], array_keys($themes_arr)[0]);
    }

    $form_state->setRebuild();
    return $form;
  }

  /**
   * Removes a mode set in the triggering element from the form.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   *
   * @return array
   *   The settings form.
   */
  public static function removeMode(array $form, FormStateInterface $form_state) {
    $modes_arr = static::getModes($form_state);
    $mode_key = $form_state->getTriggeringElement()['#attributes']['mode_key'];

    unset($modes_arr[$mode_key]);
    $form_state->setValue([
      'editor',
      'settings',
      'modes',
      'available_modes',
      'mode_values',
    ], $modes_arr);

    if ($form_state->getValue(['editor', 'settings', 'default_mode']) === $mode_key) {
      $form_state->setValue(['editor', 'settings', 'default_mode'], array_keys($modes_arr)[0]);
    }

    $form_state->setRebuild();
    return $form;
  }

  /**
   * Removes a extra options set in the triggering element from the form.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   *
   * @return array
   *   The settings form.
   */
  public static function removeExtra(array $form, FormStateInterface $form_state) {
    $extras_arr = static::getExtras($form_state);
    $extra_key = $form_state->getTriggeringElement()['#attributes']['extra_key'];

    unset($extras_arr[$extra_key]);
    $form_state->setValue([
      'editor',
      'settings',
      'extras',
      'available_extras',
      'extra_values',
    ], $extras_arr);

    $form_state->setRebuild();
    return $form;
  }

  /**
   * Returns array of key value pairs for themes.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   * @param bool $full
   *   Whether the form state is the full form.
   *
   * @return array
   *   An associate array of available themes.
   */
  public static function getThemes(FormStateInterface $form_state, $full = FALSE) {
    $themes = [];
    $themes_values = $form_state->getValue($full ? [
      'themes',
      'available_themes',
      'theme_table',
    ] : [
      'editor',
      'settings',
      'themes',
      'available_themes',
      'theme_table',
    ]) ?? [];
    if (empty($themes_values)) {
      return $themes;
    }
    // Sort by the weight.
    uasort($themes_values, function ($a, $b) {
      return $a['weight'] <=> $b['weight'];
    });
    foreach ($themes_values as $theme) {
      $themes[$theme['key']] = $theme['name'];
    }
    return $themes;
  }

  /**
   * Returns array of key value pairs for modes.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   * @param bool $full
   *   Whether the form state is the full form.
   *
   * @return array
   *   An associate array of available modes.
   */
  public static function getModes(FormStateInterface $form_state, $full = FALSE) {
    $modes = [];
    $modes_values = $form_state->getValue($full ? [
      'modes',
      'available_modes',
      'mode_table',
    ] : [
      'editor',
      'settings',
      'modes',
      'available_modes',
      'mode_table',
    ]) ?? [];
    if (empty($modes_values)) {
      return $modes;
    }
    // Sort by the weight.
    uasort($modes_values, function ($a, $b) {
      return $a['weight'] <=> $b['weight'];
    });
    foreach ($modes_values as $mode) {
      $modes[$mode['key']] = $mode['name'];
    }
    return $modes;
  }

  /**
   * Returns array of key value pairs for extra options.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the form.
   * @param bool $full
   *   Whether the form state is the full form.
   *
   * @return array
   *   An associate array of available extra options.
   */
  public static function getExtras(FormStateInterface $form_state, $full = FALSE) {
    $extras = [];
    $extras_values = $form_state->getValue($full ? [
      'extras',
      'available_extras',
      'extra_table',
    ] : [
      'editor',
      'settings',
      'extras',
      'available_extras',
      'extra_table',
    ]) ?? [];
    if (empty($extras_values)) {
      return $extras;
    }
    // Sort by the weight.
    uasort($extras_values, function ($a, $b) {
      return $a['weight'] <=> $b['weight'];
    });
    foreach ($extras_values as $extra) {
      $extras[$extra['key']] = $extra['value'];
    }
    return $extras;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    $libs = ['inline_formatter_field/iff_ace_editor'];
    return $libs;
  }

  /**
   * {@inheritdoc}
   */
  public function getJsSettings(Editor $editor) {
    return $editor->getSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $form_values = $form_state->getValues();
    $settings = [
      'default_theme' => $form_values['themes']['available_themes']['theme_table'][$form_values['default_theme']]['key'],
      'default_mode' => $form_values['modes']['available_modes']['mode_table'][$form_values['default_mode']]['key'],
      'available_themes' => static::getThemes($form_state, TRUE),
      'available_modes' => static::getModes($form_state, TRUE),
      'available_extras' => static::getExtras($form_state, TRUE),
      'clear_tokens' => $form_values['clear_tokens'],
    ];
    $form_state->setValues($settings);
  }

}
