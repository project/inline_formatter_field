<?php

namespace Drupal\inline_formatter_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'html_inject_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "inline_formatter_field_formatter",
 *   label = @Translation("Inline Formatter Field Formatter"),
 *   field_types = {
 *     "inline_formatter_field",
 *     "boolean",
 *   }
 * )
 */
class InlineFormatterFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The token object.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Drupal config factory service container.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a StringFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Utility\Token $token
   *   The token object.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The handle of module objects.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   * @param Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, Token $token, ModuleHandlerInterface $module_handler, AccountProxy $current_user, ConfigFactory $config_factory, Renderer $renderer, EntityTypeManager $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->configFactory = $config_factory;
    $this->token = $token;
    $this->moduleHandler = $module_handler;
    $this->currentUser = $current_user;
    $this->renderer = $renderer;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('token'),
      $container->get('module_handler'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('renderer'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'formatted_field' => [
        'value' => '<h1>Hello World!</h1>',
      ],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form = parent::settingsForm($form, $form_state);

    if ($this->currentUser->hasPermission('edit inline formatter field formats')) {
      $config = $this->configFactory->get('inline_formatter_field.settings');
      $editor = $config->get('default_editor');
      $default_value = $this->getSetting('formatted_field');
      // For the migration from string to array.
      if (gettype($default_value) === 'array') {
        $default_value = $default_value['value'];
      }

      $form['formatted_field'] = [
        '#type' => 'text_format',
        '#title' => $this->t('Inline Formatter Field Formatter'),
        '#format' => $editor,
        '#allowed_formats' => [$editor],
        '#default_value' => $default_value,
        '#element_validate' => [[$this, 'formattedFieldValidate']],
      ];
    }

    return $form;
  }

  /**
   * Replaces \r\n with \n so multiline yaml works.
   */
  public function formattedFieldValidate($element, $form_state, $form) {
    if (isset($element['#value'])) {
      $value = $element['#value'];
      $value = preg_replace('/\r\n?/', "\n", $value);
      $form_state->setValueForElement($element, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary_format = $this->getSetting('formatted_field');
    // For migrating from string to array.
    if (gettype($summary_format) === 'array') {
      $summary_format = $summary_format['value'];
    }
    $format_arr = array_slice(explode(PHP_EOL, $summary_format), 0, 6);

    foreach ($format_arr as $key => $line) {
      if ($key < 5) {
        $summary[] = strlen($line) > 50 ? substr($line, 0, 50) . "..." : $line;
      }
      else {
        $summary[] = "...";
      }
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $settings = $this->getSettings();
    $formatted_field = $settings['formatted_field'];
    $config = $this->configFactory->get('inline_formatter_field.settings');
    $default_editor = $config->get('default_editor');
    $editor = $this->entityTypeManager->getStorage('editor')->load($default_editor);

    // Render the processed text to get the HTML.
    if (gettype($formatted_field) === 'array' && isset($formatted_field['format'])) {
      $editor = $this->entityTypeManager->getStorage('editor')->load($formatted_field['format']);
      $processed_text = [
        '#type' => 'processed_text',
        '#text' => $formatted_field['value'],
        '#format' => $formatted_field['format'],
        '#langcode' => $langcode,
      ];
      $formatted_field = $this->renderer->render($processed_text);
    }

    foreach ($items as $delta => $item) {
      if ($item->display_format || $item->value) {
        // Get entity.
        $entity = $item->getEntity();

        // Replace tokens.
        $token_data = [
          $entity->getEntityTypeId() => $entity,
        ];
        $clear_tokens = $editor->getSettings()['clear_tokens'] ?? FALSE;
        $formatted_field = $this->token->replace($formatted_field, $token_data, ['clear' => $clear_tokens]);

        $context = [
          $entity->getEntityTypeId() => $entity,
          'current_user' => $this->currentUser,
        ];
        $this->moduleHandler->alter('inline_formatter_field_formatter_context', $context, $entity);

        // Uses the inline template feature from Drupal to be able to use Twig.
        $element[$delta] = [
          '#type' => 'inline_template',
          '#template' => $formatted_field,
          '#context' => $context,
        ];
      }
    }

    return $element;
  }

}
