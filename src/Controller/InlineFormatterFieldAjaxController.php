<?php

namespace Drupal\inline_formatter_field\Controller;

use Drupal\Core\Ajax\AfterCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\BeforeCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Inline Formatter Field routes.
 */
class InlineFormatterFieldAjaxController extends ControllerBase {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(RendererInterface $renderer, ModuleHandlerInterface $module_handler) {
    $this->renderer = $renderer;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('module_handler')
    );
  }

  /**
   * Builds the Ace Editor and sends as a response.
   */
  public function render(Request $request) {
    $response = new AjaxResponse();
    $data = $request->request->all();

    $element = $data['element'];
    $settings = $data['settings'];

    // Add settings details.
    $response->addCommand(new BeforeCommand($element, $this->getPersonalSettingsForm($settings)));
    // Check to add token tree link.
    if ($this->moduleHandler->moduleExists('token')) {
      $response->addCommand(new AfterCommand($element, $this->getTokenBrowser()));
    }

    return $response;
  }

  /**
   * Returns a form of the Ace Editor settings for personalizing.
   */
  public function getPersonalSettingsForm($settings) {
    // @todo Why default values don't work? Have to use value.
    $element = [];
    $element['ace_options'] = [
      '#type' => 'details',
      '#title' => t("Ace Editor Options"),
      '#attributes' => [
        'class' => ['ace-editor-personal-settings'],
      ],
    ];
    $element['ace_options']['theme'] = [
      '#type' => 'select',
      '#title' => t('Theme'),
      '#options' => $settings['available_themes'],
      // '#default_value' => $settings['default_theme'],
      '#value' => $settings['default_theme'],
      '#attributes' => [
        'class' => ['ace-theme-select'],
      ],
    ];
    $element['ace_options']['mode'] = [
      '#type' => 'select',
      '#title' => t('Mode'),
      '#options' => $settings['available_modes'],
      // '#default_value' => $settings['default_mode'],
      '#value' => $settings['default_mode'],
      '#attributes' => [
        'class' => ['ace-mode-select'],
      ],
    ];
    $element['ace_options']['extra'] = [
      '#type' => 'fieldset',
      '#title' => t("Extra Options"),
    ];
    $element['ace_options']['extra']['help'] = [
      '#markup' => t('Options can be found on <a href=":url" target="_blank">Configuring Ace</a>.<br>Booleans should be entered "true" for true and "false" for false.', [
        ':url' => 'https://ajaxorg.github.io/ace-api-docs/interfaces/Ace.EditorOptions.html',
      ]),
    ];
    foreach ($settings['available_extras'] as $option => $value) {
      $element['ace_options']['extra'][$option] = [
        '#type' => 'textfield',
        '#title' => $option,
        // '#default_value' => $value,
        '#value' => $value,
        '#attributes' => [
          'class' => ['ace-option-field'],
          'data-option-name' => $option,
        ],
      ];
    }

    return $element;
  }

  /**
   * Returns a the token tree link.
   */
  public function getTokenBrowser() {
    return [
      '#theme' => 'token_tree_link',
      '#token_types' => 'all',
      '#global_types' => FALSE,
      '#dialog' => TRUE,
    ];
  }

}
