// JS to for the iff_ace_editor.
((Drupal) => {
  Drupal.editors.iff_ace_editor = {
    attach: function attach(field, format) {
      if (typeof Drupal.wysiwyg === 'undefined') {
        Drupal.wysiwyg = {
          activeId: null,
          instances: {},
        };
      }
      const editor = new window.IffAceEditor(field, format.editorSettings);
      this.editor = editor;
    },
    detach: function detach(_element, _format, trigger) {
      if (!trigger || trigger === 'unload') {
        this.editor.delete();
      }
    },
    onChange: (_field, cb) => {
      if (cb && typeof cb === 'function') {
        cb();
      }
    },
  };
})(Drupal);
