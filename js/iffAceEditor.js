/**
 * @file
 */
((ace, Drupal, once) => {
  class IffAceEditor {
    constructor(field, settings) {
      this.field = field;
      this.mode = localStorage.getItem('ace_mode') ?? settings.default_mode;
      this.theme = localStorage.getItem('ace_theme') ?? settings.default_theme;
      this.options = settings.available_extras;
      Object.keys(this.options).forEach((option) => {
        this.options[option] =
          localStorage.getItem(`ace_${option}`) ?? this.options[option];
        if (this.options[option].toString().toLowerCase() === 'true') {
          this.options[option] = true;
        }
        if (this.options[option].toString().toLowerCase() === 'false') {
          this.options[option] = false;
        }
      });
      // Hide actual field.
      this.field.style.display = 'none';
      // Add wrapper.
      const fieldWrapper = document.createElement('div');
      fieldWrapper.classList.add('ace-editor-field-wrapper');
      field.after(fieldWrapper);
      // Add ace editor div.
      const aceEditorWrapper = document.createElement('div');
      aceEditorWrapper.classList.add('ace-editor-editor-wrapper');
      fieldWrapper.append(aceEditorWrapper);
      const aceEditorElem = document.createElement('div');
      aceEditorElem.classList.add('ace-editor-editor');
      aceEditorWrapper.append(aceEditorElem);
      // Add full screen button.
      const fullScreenButton = document.createElement('button');
      fullScreenButton.type = 'button';
      fullScreenButton.classList.add('ace-editor-fullscreen-button');
      aceEditorWrapper.append(fullScreenButton);
      fullScreenButton.addEventListener(
        'click',
        this.toggleFullscreen.bind(this),
      );
      // When 'esc' key is pressed, exit the full screen mode.
      document.addEventListener('keyup', (e) => {
        if (
          e.key === 'Escape' &&
          document
            .querySelector(`#${this.field.id} ~ .ace-editor-field-wrapper`)
            .classList.contains('fullscreen')
        ) {
          this.toggleFullscreen();
        }
      });
      // Set up the ace editor.
      this.editor = ace.edit(aceEditorElem, this.options);
      this.editor.session.setValue(this.field.value);
      this.editor.setTheme(this.theme);
      this.editor.session.setMode(this.mode);
      this.editor.session.on('change', () => {
        this.field.value = this.editor.session.getValue();
      });
      Drupal.wysiwyg.instances[this.field.id] = this.editor;
      this.editor.on('focus', this.onFocus.bind(this));
      // Label on click.
      document
        .querySelector(`label[for='${this.field.id}']`)
        .addEventListener('click', this.focus.bind(this));
      // Call ajax to render settings detail and token.
      const ajaxObject = Drupal.ajax({
        url: '/inline-formatter-field/ajax',
        element: fieldWrapper,
        submit: {
          settings,
          element: `#${field.id} ~ .ace-editor-field-wrapper .ace-editor-editor-wrapper`,
        },
      });
      ajaxObject.commands.insert = (ajax, response, status) => {
        // Call original method.
        const ajaxProto = Object.getPrototypeOf(ajaxObject.commands);
        ajaxProto.insert(ajax, response, status);
        // After inserted call function to bind setting updates.
        this.bindSettingEvents();
      };
      ajaxObject.execute();
    }

    toggleFullscreen() {
      document
        .querySelector(`#${this.field.id} ~ .ace-editor-field-wrapper`)
        .classList.toggle('fullscreen');
      // Send the resize even so that Ace Editor will resize its height.
      window.dispatchEvent(new Event('resize'));
    }

    onFocus() {
      Drupal.wysiwyg.activeId = this.field.id;
    }

    focus() {
      this.editor.focus();
    }

    updateTheme(theme) {
      this.theme = theme;
      window.localStorage.setItem('ace_theme', theme);
      this.editor.setTheme(theme);
    }

    updateMode(mode) {
      this.mode = mode;
      window.localStorage.setItem('ace_mode', mode);
      this.editor.session.setMode(mode);
    }

    updateOption(option, value) {
      let newValue = value;
      if (newValue.toString().toLowerCase() === 'true') {
        newValue = true;
      }
      if (newValue.toString().toLowerCase() === 'false') {
        newValue = false;
      }
      this.options[option] = newValue;
      window.localStorage.setItem(`ace_${option}`, newValue);
      this.editor.setOption(option, newValue);
    }

    bindSettingEvents() {
      once(
        'ace-editor-settings-binding',
        `#${this.field.id} ~ .ace-editor-field-wrapper .ace-editor-personal-settings`,
      ).forEach((settingsDetail) => {
        const themeSelect = settingsDetail.querySelector('.ace-theme-select');
        const modeSelect = settingsDetail.querySelector('.ace-mode-select');
        const optionFields =
          settingsDetail.querySelectorAll('.ace-option-field');
        // Initial values.
        themeSelect.value = this.theme;
        modeSelect.value = this.mode;
        // On change events.
        themeSelect.addEventListener('change', (e) => {
          const theme = e.target.value;
          this.updateTheme(theme);
        });
        modeSelect.addEventListener('change', (e) => {
          const mode = e.target.value;
          this.updateMode(mode);
        });
        // For each options.
        optionFields.forEach((optionField) => {
          const option = optionField.getAttribute('data-option-name');
          // Initial value
          optionField.value = this.options[option];
          // On change.
          optionField.addEventListener('change', (e) => {
            const { value } = e.target;
            this.updateOption(option, value);
          });
        });
      });
    }

    delete() {
      // Show actual field.
      this.field.style.display = 'block';
      // Label on click.
      document
        .querySelector(`label[for='${this.field.id}']`)
        .removeEventListener('click', this.focus.bind(this));
      // Remove the added ace editor.
      document
        .querySelector(`#${this.field.id} ~ .ace-editor-field-wrapper`)
        ?.remove();
    }
  }
  window.IffAceEditor = IffAceEditor;
})(ace, Drupal, once);
