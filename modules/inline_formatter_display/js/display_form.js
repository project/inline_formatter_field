((once, Drupal) => {
  // Since field_ui_table does not support #states we have to toggle visibility
  // with our own JS.
  Drupal.behaviors.inline_formatter_display_form_display = {
    attach: () => {
      once(
        'inline_formatter_display_form_display',
        '.iff-display-checkbox',
      ).forEach((checkbox) => {
        // Get elements
        const iffWrapper = document.querySelector('.iff-settings-wrapper');
        const fieldsWrapper = document.querySelector('.iff-fields-wrapper');

        // Set which is displayed and hidden on load
        if (checkbox.checked) {
          iffWrapper.style.display = 'block';
          fieldsWrapper.style.display = 'none';
        } else {
          iffWrapper.style.display = 'none';
          fieldsWrapper.style.display = 'block';
        }

        // Set on change event
        checkbox.addEventListener('change', (e) => {
          if (e.target.checked) {
            iffWrapper.style.display = 'block';
            fieldsWrapper.style.display = 'none';
          } else {
            iffWrapper.style.display = 'none';
            fieldsWrapper.style.display = 'block';
          }
        });
      });
    },
  };
})(once, Drupal);
