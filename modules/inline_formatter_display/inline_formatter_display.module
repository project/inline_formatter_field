<?php

/**
 * @file
 * Contains inline_formatter_display.module.
 */

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\field_ui\Form\EntityViewDisplayEditForm;

/**
 * Implements hook_help().
 */
function inline_formatter_display_help($route_name, RouteMatchInterface $route_match) {
  $output = '';
  switch ($route_name) {
    // Help for the inline formatter display module.
    case 'help.page.inline_formatter_display':
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t("The Inline Formatter Display module allows for templating and styling of entities by replacing an entity's manage display form with an editor field for writing an inline format.") . '</p>';
      break;
  }
  return $output;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function inline_formatter_display_form_entity_view_display_edit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form_object = $form_state->getFormObject();
  if ($form_object instanceof EntityViewDisplayEditForm) {
    // Get view mode entity.
    $entity = $form_object->getEntity();
    if ($entity instanceof EntityViewDisplay) {
      // Add inline formatter libraries.
      $form['#attached']['library'][] = 'inline_formatter_display/display_form';
      // Wrappers for toggling displays.
      $form['fields']['#prefix'] = "<div class='iff-fields-wrapper'>";
      $form['fields']['#suffix'] = "</div>";
      $form['inline_formatter_display']['#prefix'] = '<div class="iff-settings-wrapper">';
      $form['inline_formatter_display']['#suffix'] = '</div>';
      // Get use value.
      $use_value = $entity->getThirdPartySetting('inline_formatter_display', 'use', 0);
      // Add save function for inline formatter fields on save.
      array_unshift($form['actions']['submit']['#submit'], 'inline_formatter_display_save');

      if (Drupal::service('current_user')->hasPermission('edit inline formatter display formats')) {
        // Get inline formatter field configuration settings.
        $config = Drupal::service('config.factory')->get('inline_formatter_field.settings');
        $editor = $config->get('default_editor');
        // Get previous value.
        $display = $entity->getThirdPartySetting('inline_formatter_display', 'formatted_display', [
          'value' => '<h1>Hello World!</h1>',
          'format' => $editor,
        ]);

        if (gettype($display) === 'string') {
          $display = [
            'value' => $display,
            'format' => $editor,
          ];
        }

        // The actual textarea field.
        $form['inline_formatter_display']['inline_formatter_display_formatted_display'] = [
          '#type' => 'text_format',
          '#title' => t('Inline Formatter Field Formatter'),
          '#format' => $editor,
          '#allowed_formats' => [$editor],
          '#default_value' => $display['value'],
        ];
      }
      else {
        $form['inline_formatter_display']['message'] = [
          '#type' => 'inline_template',
          '#template' => '<p>You do not have permission to edit the inline format of this display.</p>',
        ];
      }
      // Button for toggling whether to use the formatter display.
      $form['inline_formatter_display_use'] = [
        '#type' => 'checkbox',
        '#title' => t('Use Inline Formatter Display'),
        '#default_value' => $use_value,
        '#attributes' => [
          'class' => ['iff-display-checkbox'],
        ],
      ];
    }
  }
}

/**
 * Saves the inline formatter display settings for the entity.
 */
function inline_formatter_display_save(&$form, FormStateInterface $form_state) {
  $form_object = $form_state->getFormObject();
  if ($form_object instanceof EntityViewDisplayEditForm) {
    // Get view mode entity.
    $entity = $form_object->getEntity();
    if ($entity instanceof EntityViewDisplay) {
      // Save the fields.
      $use_value = $form_state->getValue('inline_formatter_display_use');
      $display = $form_state->getValue('inline_formatter_display_formatted_display');
      $entity->setThirdPartySetting('inline_formatter_display', 'use', $use_value);
      if ($display) {
        $display_value = preg_replace('/\r\n?/', "\n", $display['value']);
        $entity->setThirdPartySetting('inline_formatter_display', 'formatted_display', [
          'value' => $display_value,
          'format' => $display['format'],
        ]);
      }
    }
  }
}

/**
 * Implements hook_entity_view_alter().
 */
function inline_formatter_display_entity_view_alter(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display) {
  if ($display->getThirdPartySetting('inline_formatter_display', 'use', 0)) {
    $config = Drupal::service('config.factory')->get('inline_formatter_field.settings');
    $editorName = $config->get('default_editor');
    $display_value = $display->getThirdPartySetting('inline_formatter_display', 'formatted_display', [
      'value' => '<h1>Hello World!</h1>',
      'format' => $editorName,
    ]);
    $editor = Drupal::service('entity_type.manager')->getStorage('editor')->load($display_value['format']);
    $clear_tokens = $editor->getSettings()['clear_tokens'] ?? FALSE;
    // Render the processed text to get the HTML.
    if (gettype($display_value) === 'array') {
      $processed_text = [
        '#type' => 'processed_text',
        '#text' => $display_value['value'],
        '#format' => $display_value['format'],
      ];
      $display_value = Drupal::service('renderer')->render($processed_text);
    }
    $module_handler = Drupal::service('module_handler');
    $token_data = [
      $entity->getEntityTypeId() => $entity,
    ];
    $display_value = Drupal::service('token')->replace($display_value, $token_data, ['clear' => $clear_tokens]);
    // Remove all properties that are not entity attributes or the label key.
    $label_key = 'title';
    $entity_type_keys = $entity->getEntityType()->getKeys();
    if (array_key_exists('label', $entity_type_keys)) {
      $label_key = $entity_type_keys['label'];
    }
    foreach ($build as $key => $item) {
      if (substr($key, 0, 1) !== '#' && $key !== $label_key) {
        unset($build[$key]);
      }
    }

    $context = [
      $entity->getEntityTypeId() => $entity,
      'current_user' => Drupal::service('current_user'),
    ];
    $module_handler->alter('inline_formatter_display_context', $context, $entity);

    // Add inline template.
    $build['inline_formatter_display'] = [
      '#type' => 'inline_template',
      '#template' => $display_value,
      '#context' => $context,
    ];
  }
}
