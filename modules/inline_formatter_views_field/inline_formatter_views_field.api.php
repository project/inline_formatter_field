<?php

/**
 * @file
 * Hooks specific to the Inline Formatter Views Field module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the context array for the inline_template in views field.
 *
 * This hooks allows modules to add or adjust context for twig values in the
 * Inline Formatter Views Field.
 *
 * @param array $context
 *   The array with the current context for the inline_template.
 */
function hook_inline_formatter_views_field_context_alter(array &$context) {
  $context['language'] = \Drupal::languageManager()->getCurrentLanguage()->getId();
}

/**
 * @} End of "addtogroup hooks".
 */
