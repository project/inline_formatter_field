<?php

namespace Drupal\inline_formatter_views_field\Plugin\views\field;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Utility\Token;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Render\ViewsRenderPipelineMarkup;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A handler to provide a HTML or Twig.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("inline_formatter_views_field")
 */
class InlineFormatterViewsField extends FieldPluginBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The token object.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Drupal config factory service container.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a InlineFormatterViewsField instance.
   *
   * @param array $configuration
   *   An array of configuration for the plugin.
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The handle of module objects.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   * @param \Drupal\Core\Utility\Token $token
   *   The token object.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   * @param Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler, AccountProxy $current_user, Messenger $messenger, Token $token, ConfigFactory $config_factory, Renderer $renderer, EntityTypeManager $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $module_handler;
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
    $this->token = $token;
    $this->configFactory = $config_factory;
    $this->renderer = $renderer;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('current_user'),
      $container->get('messenger'),
      $container->get('token'),
      $container->get('config.factory'),
      $container->get('renderer'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    $options['inline_formatter_field_display'] = [
      'default' => [
        'value' => '<h1>Hello World!</h1>',
      ],
    ];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    if ($this->currentUser->hasPermission('edit inline formatter views field')) {

      $config = $this->configFactory->get('inline_formatter_field.settings');
      $editor = $config->get('default_editor');

      $default_value = $this->options['inline_formatter_field_display'];
      if (gettype($default_value) === 'array') {
        $default_value = $default_value['value'];
      }

      $form['inline_formatter_field_display'] = [
        '#type' => 'text_format',
        '#title' => $this->t('Inline Formatter Field Formatter'),
        '#format' => $editor,
        '#allowed_formats' => [$editor],
        '#default_value' => $default_value,
        '#element_validate' => [[$this, 'formattedFieldValidate']],
      ];

      $form['help'] = $form['alter']['help'];
      unset($form['help']['#states']);
    }
    else {
      $this->messenger->addWarning('You do not have permission to edit the inline format of this field.');
      $form['status_messages'] = [
        '#type' => 'status_messages',
      ];
    }
  }

  /**
   * Replaces \r\n with \n so multiline yaml works.
   */
  public function formattedFieldValidate($element, $form_state, $form) {
    if (isset($element['#value'])) {
      $value = $element['#value'];
      $value = preg_replace('/\r\n?/', "\n", $value);
      $form_state->setValueForElement($element, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $formatted_display = $this->options['inline_formatter_field_display'];
    // Render the processed text to get the HTML.
    $config = $this->configFactory->get('inline_formatter_field.settings');
    $editorName = $config->get('default_editor');
    if (gettype($formatted_display) === 'array') {
      $editorName = $formatted_display['format'];
      $processed_text = [
        '#type' => 'processed_text',
        '#text' => $formatted_display['value'],
        '#format' => $formatted_display['format'],
      ];
      $formatted_display = $this->renderer->render($processed_text);
    }
    $editor = $this->entityTypeManager->getStorage('editor')->load($editorName);
    $context = [];
    if (strpos($formatted_display, '{{') !== FALSE) {
      $fake_item = [
        'alter_text' => TRUE,
        'text' => $formatted_display,
      ];
      // Get tokens from the last field.
      $last_field = end($this->view->field);
      if (isset($last_field->last_tokens)) {
        $tokens = $last_field->last_tokens;
      }
      else {
        $tokens = $last_field
          ->getRenderTokens($fake_item);
      }
      // Update context.
      $context = $this->getContextFromTokens($tokens);
    }

    // Global token replace.
    $token_data = [];
    $clear_tokens = $editor->getSettings()['clear_tokens'] ?? FALSE;
    $formatted_display = $this->token->replace($formatted_display, $token_data, ['clear' => $clear_tokens]);

    // Call the hook for updating context.
    $this->moduleHandler->alter('inline_formatter_views_field_context', $context);

    // Return the Drupal\Component\Render\MarkupInterface.
    $build = [
      '#type' => 'inline_template',
      '#template' => $formatted_display,
      '#context' => $context,
    ];
    $build_str = (string) $this
      ->getRenderer()
      ->render($build);
    return ViewsRenderPipelineMarkup::create($build_str);
  }

  /**
   * Expands the given tokens into a context array.
   */
  protected function getContextFromTokens($tokens) {
    $twig_tokens = [];
    foreach ($tokens as $token => $replacement) {
      if (strpos($token, '{{') !== FALSE) {
        $token = trim(str_replace([
          '{{',
          '}}',
        ], '', $token));
      }
      if (strpos($token, '.') === FALSE) {
        // We need to validate tokens are valid Twig variables. Twig uses the
        // same variable naming rules as PHP.
        // @see http://php.net/manual/language.variables.basics.php
        assert(preg_match('/^[a-zA-Z_\\x7f-\\xff][a-zA-Z0-9_\\x7f-\\xff]*$/', $token) === 1, 'Tokens need to be valid Twig variables.');
        $twig_tokens[$token] = $replacement;
      }
      else {
        $parts = explode('.', $token);
        $top = array_shift($parts);
        assert(preg_match('/^[a-zA-Z_\\x7f-\\xff][a-zA-Z0-9_\\x7f-\\xff]*$/', $top) === 1, 'Tokens need to be valid Twig variables.');
        $token_array = [
          array_pop($parts) => $replacement,
        ];
        foreach (array_reverse($parts) as $key) {
          // The key could also be numeric (array index) so allow that.
          assert(is_numeric($key) || preg_match('/^[a-zA-Z_\\x7f-\\xff][a-zA-Z0-9_\\x7f-\\xff]*$/', $key) === 1, 'Tokens need to be valid Twig variables.');
          $token_array = [
            $key => $token_array,
          ];
        }
        if (!isset($twig_tokens[$top])) {
          $twig_tokens[$top] = [];
        }
        $twig_tokens[$top] += $token_array;
      }
    }
    return $twig_tokens;
  }

}
