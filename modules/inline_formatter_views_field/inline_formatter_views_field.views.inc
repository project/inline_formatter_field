<?php

/**
 * @file
 * Provide views data for inline formatter views field module.
 */

/**
 * Implements hook_views_data().
 */
function inline_formatter_views_field_views_data() {
  $data['views']['table']['group'] = t('Global');
  $data['views']['table']['join'] = [
    // #global is a special flag which allows a table to appear all the time.
    '#global' => [],
  ];

  $data['views']['inline_formatter'] = [
    'title' => t('Inline Formatter'),
    'help' => t('Provide custom HTML or Twig.'),
    'field' => [
      'id' => 'inline_formatter_views_field',
      'click sortable' => FALSE,
    ],
  ];

  return $data;
}
