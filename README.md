![IFF Logo](./logo.svg)
# Inline Formatter Field

## Introduction

The Inline Formatter Field module allows site builders to template and style 
entities with a field. This module will create a new field type called "Inline
Formatter" which is a boolean. When the boolean is checked the field will render
what is entered in the "HTML or Twig Format" field for the formatter's settings 
in the "Manage Display" tab of the entity. This module makes use of the ACE
Editor JavaScript library for better user experience in the browser. As the name
suggests, any HTML or Twig can be entered into this field.

Two Twig variables can be used to display dynamic content. First, the entity
itself is accessed by the variable named by the entity type (`node`, `media`,
`block_content`, etc), which can be used for obtaining field information or any
other entity information. Second, the current user is accessed by the variable
`current_user`, which can be used to display personalized information or check
user roles. View
[common twig patterns](https://www.drupal.org/docs/contributed-modules/inline-formatter-field/common-twig-patterns)
documentation page to see more specifics use cases.

The IFF field basically is an on and off switch (boolean) which when turned on
can render whatever the site builder sets and wherever the field is placed on
the entity's display.

For many more use cases it is recommended to use the Inline Formatter Display
submodule. This submodule allows the site builder to completely control what is
rendered on an entity per bundle. Essentially, creates an entity display twig
template configured in the manage display tab in the browser.

## Requirements

This module requires Drupal core version 9.2 or higher.

This module requires the following module:
  - drupal:field - The Drupal core field module.
  - drupal:editor - The Drupal core editor module.
  - drupal:filter - The Drupal core filter module.

## Installation

1. Install and enable this module as you would normally
[install any contributed Drupal module](https://www.drupal.org/docs/extending-drupal/installing-modules).
2. Add the field to the desired entity(ies).
3. Edit the manage display form for the entity for the inline formatter to
display what is desired.

Upon installing this module a new text editor will be created, "IFF Ace Editor"
(iff_ace_editor). This editor is used as default for the Inline Formatter Field.
The editor can be swapped in the Inline Formatter Field Settings Form
(/admin/config/inline_formatter_field/settings). See below for details on
configurations.

## Configuration

This module has configuration for the following items:
- default_editor - The editor to use for the Inline Formatter Field templating.
- ace_source - The path or URL to get the Ace Editor library.
- ace_theme - The Ace Editor theme to be used by default.
- ace_mode - The Ace Editor mode to be used by default.
- available_themes - A list of key value pairs of available Ace Editor themes.
- available_modes - A list of key value pairs of available Ace Editor modes.
- extra_options - A list of key value pairs of Ace Editor extra settings.

These configuration values can be altered in the Inline Formatter Field Settings
Form (admin/config/inline_formatter_field/settings).

The Ace Editor theme, mode, and extra settings are the default values, and each
user can alter their preference on the manage display form. The user's values
are saved as a cookie for the site. If no cookie is found, then the default is
used.

---

This module creates an editor called "IFF Ace Editor" (iff_ace_editor). This
editor is creating to use the iff_ace_editor Ace Editor plugin. This editor can
be configured as any other editor with filters.

---

This module creates a field type with the following item:
- formatted_field - The HTML or Twig to render when the field is checked for 
display.

This configuration value can be altered in the manage display tab of the entity.

## Submodules

This module has a few submodules that extend the concepts of this base module.

### Inline Formatter Display

The Inline Formatter Display module will allow you to override the display of an
entity with the processed content of a text editor. First, on the entity's
manage display tab check the box for "Use Inline Formatter Display". This will
remove the normal display form and replace it with a text editor field, the IFF
Ace Editor by default. When saved the contents entered in the text field will be
what is rendered on each entity of that type and bundle. The use of tokens and
twig can be entered to render dynamic content per entity.

Essentially, this module gives you a way to make an entity bundle twig template
without the need of actually adding files, changing a theme, or creating a child
theme. You can add whatever HTML/twig you like to an entity's display.

There are many use cases for this feature of which includes:
- Adding static HTML not associated with the entity's data.
- Altering a field's display.
- Rendering a field in more than once and more than one way.
- Conditionally rendering a field based on other data such as another field.
- Adding blocks or other rendered entities to another entity's display.
- And, more!

As with the IFF field, twig can and probably should be used to display different
content per entity. The twig variable named after the entity type the display is
for is available in order to access field values. See
[common twig patterns](https://www.drupal.org/docs/contributed-modules/inline-formatter-field/common-twig-patterns)
for more details.

This is the recommended way to change the display of an entity rather than
adding an IFF field and following
[this article](https://www.drupal.org/docs/8/modules/inline-formatter-field/use-field-for-full-entity-display).

### Inline Formatter Views Field

The Inline Formatter Views Field module creates a new field type that is
available for Drupal views called "Inline Formatter" in the global category.
This field is similar to the core "Custom text" views field except that this
field uses the text editor, which results in a better user experience. This can
be useful to not filter out things that are filtered by the "Custom text" since
the filter can be controlled through the text editor. It is important to note
the security concerns this could cause and to only give permission to trusted
roles as you would with any text editor without "Limit allowed HTML tags and
correct faulty HTML" checked.

## Extending this Module

This module has a hook `hook_inline_formatter_field_formatter_context_alter`
which can be used to add more variables into the twig context of the IFF field.
View inline_formatter_field.api.php for the example of adding `label` and
`language` contexts. Other use cases can be done similarly.

The submodules Inline Formatter Display and Inline Formatter Views Field both
have similar hooks `hook_inline_formatter_display_context_alter` and
`hook_inline_formatter_views_field_context_alter`, respectively, which can be
used to add to the context of the twig in the same way.

## Upgrades

It is advised to always backup your site before updating, especially with major
updates! After updating this module, please run any update hooks necessary
either with `drush updb` or through the admin page. After running the hooks
please check configuration and test your site.

### Upgrading to Version 4

Upgrading to version 4 will create a new text editor, IFF Ace Editor, which will
be used for user editing experience.

This version now requires Drupal 9.2 or higher. This is because this version
removed any dependency on jQuery, including the jQuery.once, which was replaced
with the Drupal core's once library, which was introduced in Drupal 9.2.

There were several configuration updates made with this version. The
configuration for the default editor was added. The configuration for
clear_tokens was moved to the editor schema. The configuration for FontAwesome
was removed.

The libraries for this module were also refactored. Some libraries were removed,
some added, some had a name change along with dependencies.

### Upgrading to Version 3

Upgrading to version 3 will add more permissions for the two submodules. After
updating you may need to configure permissions for these modules if any role
besides admin needs access to use them.

This upgrade also introduced the hooks for extending the context.

### Upgrading to Version 2

The upgrade to version 2 of this module changed configuration settings. It is
advised to back up first and then manually inspect configuration after updating
and running the update hooks.

## Troubleshooting

If the field is not rendering, check to see if the boolean field is actually
checked in the edit form for the content, and check to make sure that valid
html and twig is entered. You may also need to check if the text editor has any
filters set which could affect the rendered display.

---

If the ACE Editor fails to load:
- Make sure that JavaScript is allowed in your browser, and check for console
errors.
- Check that the library path set in the module's settings exists and is
correctly entered. If a CDN is used, make sure the URL is correct.

---

For reporting any errors or bugs, please
[create an issue](https://www.drupal.org/project/issues/inline_formatter_field).

## FAQ

Q: I checked the box in the content form and all that is rendering is a h1
"Hello World!". What am I missing?

A: The "Hello World!" message is the default template. In order to change this,
go to the manage display tab for the entity and click on the gear for the inline
formatter field. Then, replace the "Hello World!" with your own template.

---

Q: Can I use more than one of these on a single entity type?

A: Yes, you can use multiple inline formatter fields to a single entity bundle.
This will allow you to have many different "displays" or formats for a single
entity by checking which display you want, or you could display multiple parts
of a rendered entity with separate inline formatter fields.

---

Q: Will the format render if the checkbox is not checked?

A: No, the checkbox must be selected in order for the format to be rendered.
This will allow the ability for part of a template to either render or not
render based on the content creator.

---

Q: Can I use this to display dynamic content?

A: Yes, Twig can be entered into the "HTML or Twig Format" manage display field.
The current user and entity along with all its fields can be used in this
display field.

---

Q: Can I hide the field and always make it checked?

A: Yes, when adding the field, you can check the box for the default value. Now,
you can move the field to the disabled section in the manage form tab. This will
force all new content with this entity to have this field checked by default.
Note, that all existing content won't change. 

This strategy can be used to make a field which a certain display to be moved
around in the entity manage display tab. Similar to how the "Extra field" module
works.

## Recommended Modules

- [Token](https://www.drupal.org/project/token) - When enabled, more token
replacement patterns can be entered into the formatter field. Also, this module
gives an easy interface to get entity variables.
- [Twig Tweak](https://www.drupal.org/project/twig_tweak) - This module makes
more Drupal specific twig functions which can be used in the formatter. Some
useful functions/filters include `drupal_block`, `drupal_entity`,
`drupal_field`, `drupal_image`, and more!

## Alternative Modules

- [Field Token Value](https://www.drupal.org/project/field_token_value) - This
module creates a Field API type and widget which allows a field to be created
with the value containing tokens.
- [Computed field](https://www.drupal.org/project/computed_field) - Computed
Field is a very powerful field module that lets you add a custom "computed
fields" to your content types.
- [Extra field](https://www.drupal.org/project/extra_field) - Provides plugin
types to add extra fields in entity view and entity form of fieldable entities.
- [Twig field](https://www.drupal.org/project/twig_field) - A simple module that
provides Twig field type.
- [Filter Twig](https://www.drupal.org/project/filter_twig) - This is a very
simple module to make twig values available as an input filter.
- [Markup field](https://www.drupal.org/project/markup_field) - A simple field
supposed to contain rendered markup and all related assets (css/js).
- [Ace Code Editor](https://www.drupal.org/project/ace_editor) - This module
integrates the Ace editor into Drupal's node/block edit forms, for editing raw
HTML, PHP, JS, etc... in a familiar way.

## Maintainers

Current maintainers:
  - [Bobby Saul](https://www.drupal.org/u/bobbysaul)
