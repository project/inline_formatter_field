<?php

/**
 * @file
 * Hooks specific to the Inline Formatter Field module.
 */

use Drupal\Core\Entity\EntityInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the context array for the inline_template in field formatter.
 *
 * This hooks allows modules to add or adjust context for twig values in the
 * Inline Formatter Field field formatter.
 *
 * @param array $context
 *   The array with the current context for the inline_template.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity the .
 */
function hook_inline_formatter_field_formatter_context_alter(array &$context, EntityInterface $entity) {
  $context['label'] = $entity->label();
  $context['language'] = \Drupal::languageManager()->getCurrentLanguage()->getId();
}

/**
 * @} End of "addtogroup hooks".
 */
